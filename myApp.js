var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {
    $scope.firstName= "John";
    $scope.lastName= "Doe";
    this.currentDate = new Date()
    this.events = events    

    $scope.isUpcoming = function(currentDate,eventDate){
    if (currentDate < eventDate)
        return 'Event Date is Upcoming';
    else
        return 'Event has passed';
}
});

var events = [
	{
    	name: "Winter Tea", 
    	date: new Date(2017, 01, 22)  
    },
    {
    	name: "Garden Tea", 
    	date: new Date(2017, 03, 17)  
    },
    {
    	name: "Summer Tea", 
    	date: new Date(2017, 06, 10)  
    } 
    ]

